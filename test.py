from pytube import YouTube


# = input('Enter a youtube video URL: ')
url = "https://www.youtube.com/watch?v=rf_KqDoNvhQ&"
print(url)
YT = YouTube(url)

# Size of file in Bytes
sizeofFile = YouTube(url).streams.first().filesize


title = YouTube(url).title
print("TITLE: ", title)

AmountOfChunks = sizeofFile / 1000000
print("FILE SIZE: ", round(AmountOfChunks), "MB")

YouTube(url).streams.first().download()
