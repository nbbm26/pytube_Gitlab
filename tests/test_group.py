from pytube import YouTube

def test_filesize():
    url = "https://www.youtube.com/watch?v=rf_KqDoNvhQ&"

    # Size of file in Bytes
    actual_filesize = YouTube(url).streams.first().filesize

    expected_file_size = 52408985

    assert actual_filesize == expected_file_size
